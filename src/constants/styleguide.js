/**
 *  VanEsch Styleguide define const values here
 */

/*
 * Sizes
 */
export const SIZES = {
    logo: {
        width: '160px',
        height: '60px'
    }
};


/**
 * colors
 */
export const COLORS = {
    black: '#000000',
    quarts: '#1A1A1A',
    ruby: '#EB0000',
    snow: '#FAF9FA',
    white: '#FFFFFF',
    grey: '#D9D9D9',
};

/**
 * Fonts
 */
const FONTS = {
    Gill: {
        light: '200 1rem/1 Gill Sans Nova, Gill Sans Nova W01 Light, sans-serif',
        book: '300 1rem/1 Gill Sans Nova, Gill Sans Nova W01 Book, sans-serif',
        medium: '400 1rem/1 Gill Sans Nova, Gill Sans Nova W01 Medium, sans-serif',
        semibold: '500 1rem/1 Gill Sans Nova, Gill Sans Nova W01 Semi-Bold, sans-serif',
        bold: '600 1rem/1 Gill Sans Nova, Gill Sans Nova W01 Bold, sans-serif',
    },
    Helvetica: {
        light: '200 1rem/1 Helvetica Neue, sans-serif',
        regular: '400 1rem/1 Helvetica Neue, sans-serif',
        medium: '500 1rem/1 Helvetica Neue, sans-serif',
    }
};
FONTS.primary = FONTS.Gill;
FONTS.secondary = FONTS.Helvetica;
export { FONTS };

/**
 * Font sizes
 */

 // Helper function from px to vw
export const pxToVw = (size, screen = 1920) => `${size / screen * 100}vw`;

/*
 * All available fontsizes
 * - See end of file for font definitions based on design. (1.)
 */
const FONT_SIZE = {
    xxl: '2.5rem',
    xl: '1.5rem',
    l: '1.125rem',
    m: '1rem',
    s: '0.8125rem',
    xs: '0.6875rem',
    xxs: '0.625rem'
};

FONT_SIZE.navigation = FONT_SIZE.s;
FONT_SIZE.title = FONT_SIZE.xxl;
FONT_SIZE.subtitle = FONT_SIZE.m;
FONT_SIZE.product = {
    title: FONT_SIZE.xxl,
    price: FONT_SIZE.xl
};

export { FONT_SIZE };

// Breakpoints
export const BREAKPOINTS = {
    mobile: '767px',
    tablet: '1024px',
    desktop: '1025px',

    navigation: '1040px',
    navigationDesktop: '1041px'
};