import React from 'react';
import express from 'express';
import { renderToString } from 'react-dom/server';
import { StaticRouter, matchPath } from 'react-router-dom';
import { ServerStyleSheet, StyleSheetManager } from 'styled-components';
import { Provider } from 'react-redux';

import configureStore from '../config/store';

import routes from '../components/routes';

import layout from './layout';

import App from '../App';

const assets = require(process.env.RAZZLE_ASSETS_MANIFEST);
const sheet = new ServerStyleSheet();

const server = express();
const store = configureStore();

server
    .disable('x-powered-by')
    .use(express.static(process.env.RAZZLE_PUBLIC_DIR))
    .get('/*', (req, res) => {
        const context = {};
        const styledComponentsCss = sheet.getStyleTags();
            
        const promises = routes.map( (route) => {
            const {component, path} = route;
            const match = matchPath(req.url, route);
            return component.loadData && match ? component.loadData( store.dispatch ) : Promise.resolve(null)
        });

        Promise.all(promises).then( result => {
            const html = renderToString(
                <StyleSheetManager sheet={sheet.instance}>
                    <Provider store={store}>
                        <StaticRouter context={context} location={req.url}>
                            <App />
                        </StaticRouter>
                    </Provider>
                </StyleSheetManager>
            );

            res.status(200).send( layout({
                title: 'React Animation',
                html, 
                assets,
                css: styledComponentsCss, 
            }) );
        });

    });

export default server;
