import React from 'react';

const layout = (props) => {
    const { html, assets, css, title } = props;
    return `
        <!doctype html>
        <html lang="">
        <head>
            <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
            <meta charSet='utf-8' />
            <title>${title}</title>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            ${css}
            <script src="${assets.client.js}" defer></script>
        </head>
        <body>
            <div id="root">${html}</div>
        </body>
        </html>
    `;
};

export default layout;