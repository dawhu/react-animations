import { injectGlobal } from 'styled-components';

injectGlobal`
    * {
    box-sizing: border-box;
    }

    html, body {
        overflow: hidden;
        margin: 0;
        padding: 0;
        font-size: 16px;
    }
`;