// @flow

import React, { Component } from 'react';
import TransitionGroup from 'react-transition-group/TransitionGroup';
import { Route } from 'react-router-dom';
import initReactFastclick from 'react-fastclick';
import styled from 'styled-components';

import AnimatedSwitch from './AnimatedSwitch';

import './BaseStyling.js';

import routes from './components/routes';

// Initialize fastclick for mobile
initReactFastclick();

const Page = styled.main`
    overflow-x: hidden;
    width: 100%;
    min-height: 100%;
`;

class App extends Component {
    render() {
        return (
            <Route render={({location}) => (
                <TransitionGroup component={Page}>
                    <AnimatedSwitch key={location.key} location={location}>
                        { routes && routes.length > 0 && routes.map(( route ) => <Route key={route.path} {...route} /> ) }
                    </AnimatedSwitch>
                </TransitionGroup>
            )} />
        )
    }
}

export default App;