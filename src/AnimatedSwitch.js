import React from 'react';
import { Switch } from 'react-router-dom';
import styled from 'styled-components';
import 'gsap/TweenMax';

const TRANSITION_DURATION = 1.2;

const Page = styled.section`
    position: absolute;
    width: 100vw;
    min-height: 100vh;
`;

const Shape = styled.div`
    position: absolute;
    top: -25vw;
    left: 0;
    width: 100vw;
    height: 40vw;
    transform: translateY(0) skewY(7deg);
    background-color: rgba(0, 0, 0, 1);
`;

const PageContent = styled.section`
    position: relative;
    height: 100vh;
    padding: 15vw 4vw;

    @media (max-width: 767px) {
        padding: 30vw 20px;
    }
`;

class AnimatedSwitch extends Switch {

    page: any

    constructor(props) {
        super(props);
    }

    componentWillEnter(callback) {
        const timeline = new TimelineMax({
            onComplete: () => {
                this.page.style.zIndex = 'initial';
                callback();
            }
        });

        this.page.style.zIndex = '1';
        timeline.add( new TweenMax.fromTo(this.page, TRANSITION_DURATION, {y:'100%'}, {y:'0%', ease:Power1.easeInOut}), 0);
        timeline.add( new TweenMax.fromTo(this.content, TRANSITION_DURATION/2, {autoAlpha:0, y:-20}, {autoAlpha:1, y:0, ease:Power1.easeInOut}), TRANSITION_DURATION/2);
        timeline.add( new TweenMax.fromTo(this.shape, TRANSITION_DURATION, {y:'500%', scaleY:5}, {y:'0%', scaleY:1, ease:Power1.easeInOut}), 0);
    }

    componentWillLeave(callback) {
        const timeline = new TimelineMax({
            onComplete: callback
        });
        timeline.add( new TweenMax.to(this.page, TRANSITION_DURATION, {y:'-100%', ease:Power1.easeInOut}), 0);
        timeline.add( new TweenMax.to(this.content, TRANSITION_DURATION/2, {autoAlpha:0, y:-20, ease:Power1.easeInOut}), 0);
        // timeline.add( new TweenMax.to(this.shape, TRANSITION_DURATION/2, {skewY:-10, ease:Power1.easeInOut}), 0);
        timeline.add( new TweenMax.to(this.shape, TRANSITION_DURATION, {y:'-100%', ease:Power1.easeInOut}), 0);
    }

    render() {
        return (
            <Page innerRef={page => this.page = page}>
                <PageContent innerRef={content => this.content = content}>
                    { super.render() }
                </PageContent>
                <Shape innerRef={shape => this.shape = shape}/>
            </Page>
        )
    }
}

export default AnimatedSwitch;