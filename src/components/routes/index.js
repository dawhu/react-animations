import Overview from './Overview';
import AnimationsGSAP from './AnimationsGsap';
import AnimationsReactMotion from './AnimationsReactMotion';

export default [
    {
        path: '/',
        exact: true,
        component: Overview
    },
    {
        path: '/animations-gsap',
        exact: true,
        component: AnimationsGSAP
    },
    {
        path: '/animations-react-motion',
        exact: true,
        component: AnimationsReactMotion
    }
];