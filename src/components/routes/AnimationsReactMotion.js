// @flow

import 'gsap/TweenMax';
import React, { Component } from 'react';
import styled from 'styled-components';

import { COLORS } from '../../constants/styleguide';
import { Title, Link } from '../shared/StyledCopy';

import Header from '../shared/Header';

import Logo from '../icons/Logo';

const PositionedLogo = styled(Logo)`
    position: absolute;
    backface-visibility: hidden;
    top: 60%;
    left: 50%;
    margin: 0;
    transform: translate(-50%, -50%);
`;

const StyledList = styled.ul`
    padding: 0;
    list-style: none;
`;

const StyledListItem = styled.li`
    @media (max-width: 767px) {
        display: inline-block;
    }
`;

const StyledButton = styled.button`
    cursor: pointer;
    position: relative;
    display: block;
    width: 120px;
    margin-right: 0.5rem;
    margin-bottom: 0.5rem;
    padding: 1rem;
    border: none;
    outline: none;
    
    color: ${COLORS.snow};
    background: ${COLORS.quarts};
    
    transition: background 0.2s ease-in-out, color 0.2s ease-in-out;

    @media (min-width: 768px) {
        &:hover {
            color: ${COLORS.quarts};
            background: ${COLORS.snow};
        }
    }
`;

const Animations = [{
    type: 'loading',
    label: 'Loading',
    callback: 'doLoading'
}, {
    type: 'toggleVisible',
    label: 'Show/hide',
    callback: 'doToggleVisible'
}, {
    type: 'error',
    label: 'error',
    callback: 'doError'
}, {
    type: 'active',
    label: 'active/inactive',
    callback: 'doToggleActive'
}];

class Overview extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            isVisible: true,
            isActive: false
        }

        this.doLoading = this.doLoading.bind(this);
        this.doToggleVisible = this.doToggleVisible.bind(this);
        this.doError = this.doError.bind(this);
        this.doToggleActive = this.doToggleActive.bind(this);
    }

    doLoading() {

        if (this.state.isLoading) {
            TweenMax.fromTo(this.logo, 1, {autoAlpha:0.6}, {autoAlpha:1, repeat:-1, yoyo:true, ease:Power0.easeInOut});
            TweenMax.fromTo(this.logo, 0.9, {rotation:0}, {rotation:360, repeat:-1, ease:Power0.easeInOut});
        } else {
            TweenMax.killTweensOf(this.logo);
            TweenMax.to(this.logo, 0.6, {autoAlpha:1, rotation:360, ease:Power1.easeOut});
        }

        this.setState({
            isLoading: !this.state.isLoading
        });
    }

    doToggleVisible() {
        const posY = this.state.isVisible ? -25 : -50;
        const opacity = this.state.isVisible ? 0 : 1;
        TweenMax.to(this.logo, 0.3, {autoAlpha:opacity, y:`${posY}%`, ease:Power2.easeInOut});
        this.setState({
            isVisible: !this.state.isVisible
        });
    }

    doError() {
        TweenMax.fromTo(this.logo, 0.05, {x:'-50%'}, {x:'+=5%', repeat:3, yoyo:true, ease:Power4.easeInOut});
    }

    doToggleActive() {
        const opacity = this.state.isActive ? 1 : 0.5;
        TweenMax.to(this.logo, 0.1, {autoAlpha:opacity, ease:Quad.easeIn});
        this.setState({
            isActive: !this.state.isActive
        });
    }

    renderAnimationsList() {
        return (
            Animations.map(({type, label, callback}, index) => (
                <StyledListItem key={index}>
                    <StyledButton onClick={this[ callback ]}>{ label }</StyledButton>
                </StyledListItem>
            ))
        );
    }

    render() {

        return (
            <div>
                <Header title="Animation - React-Motion" />

                <StyledList>{ this.renderAnimationsList() }</StyledList>

                <PositionedLogo getRef={logo => this.logo = logo} width='20vw' />
            </div>
        )
    }
};


export default Overview;