// @flow

import 'gsap/TweenMax';
import React, { Component } from 'react';
import styled from 'styled-components';

import { COLORS } from '../../constants/styleguide';
import { Title, Link } from '../shared/StyledCopy';

import Header from '../shared/Header';
import AnimationWindow from '../shared/AnimationWindow';

const StyledList = styled.ul`
    padding: 0;
    list-style: none;
`;

const StyledListItem = styled.li`
    @media (max-width: 767px) {
        display: inline-block;
    }
`;

const StyledButton = styled.button`
    cursor: pointer;
    position: relative;
    display: block;
    width: 120px;
    margin-right: 0.5rem;
    margin-bottom: 0.5rem;
    padding: 1rem;
    border: none;
    outline: none;
    
    color: ${COLORS.snow};
    background: ${COLORS.quarts};
    
    transition: background 0.2s ease-in-out, color 0.2s ease-in-out;

    @media (min-width: 768px) {
        &:hover {
            color: ${COLORS.quarts};
            background: ${COLORS.snow};
        }
    }
`;

const FlexContainer = styled.div`
    display: flex;

    @media (max-width: 767px) {
        display: block;
    }
`;

const Animations = [{
    type: 'loading',
    label: 'Loading',
    callback: 'doLoading'
}, {
    type: 'toggleVisible',
    label: 'Show/hide',
    callback: 'doToggleVisible'
}, {
    type: 'error',
    label: 'error',
    callback: 'doError'
}, {
    type: 'active',
    label: 'active/inactive',
    callback: 'doToggleActive'
}, {
    type: 'drawerLeftToggle',
    label: 'Drawer left toggle',
    callback: 'doToggleDrawerLeft'
}];

class Overview extends Component {

    animationRefs: Object;

    constructor(props) {
        super(props);

        this.state = {
            easing: true,
            isLoading: true,
            isVisible: true,
            isActive: false,
            leftDrawerOpen: false
        };

        this.getRefs = this.getRefs.bind(this);

        this.doLoading = this.doLoading.bind(this);
        this.doToggleVisible = this.doToggleVisible.bind(this);
        this.doError = this.doError.bind(this);
        this.doToggleActive = this.doToggleActive.bind(this);

        this.doToggleDrawerLeft = this.doToggleDrawerLeft.bind(this);

        this.onKeyPress = this.onKeyPress.bind(this);
    }

    componentDidMount() {
        window.addEventListener('keyup', this.onKeyPress);
    }
    
    componentWillUnmount() {
        window.removeEventListener('keyup', this.onKeyPress);
    }

    onKeyPress( e ) {
        switch (e.code.toLowerCase()) {
            case 'space':
                this.setState({
                    easing: !this.state.easing
                });
                break;
        }
    }

    getRefs( refs ) {
        this.animationRefs = refs;
    }

    easing( ease ) {
        return !this.state.easing && Power0.easeNone || ease;
    }

    doLoading() {

        if (this.state.isLoading) {
            TweenMax.fromTo(this.animationRefs.logo, 1, {autoAlpha:0.6}, {autoAlpha:1, repeat:-1, yoyo:true, ease: this.easing(Power1.easeInOut)});
            TweenMax.fromTo(this.animationRefs.logo, 0.9, {rotation:0}, {rotation:360, repeat:-1, ease:this.easing(Power1.easeInOut)});
        } else {
            TweenMax.killTweensOf(this.animationRefs.logo);
            TweenMax.to(this.animationRefs.logo, 0.6, {autoAlpha:1, rotation:360, ease:Power1.easeOut});
        }

        this.setState({
            isLoading: !this.state.isLoading
        });
    }

    doToggleVisible() {
        const posY = this.state.isVisible ? 20 : 0;
        const opacity = this.state.isVisible ? 0 : 1;
        TweenMax.to(this.animationRefs.logo, 0.3, {autoAlpha:opacity, y:posY, ease:this.easing(Power2.easeInOut)});
        this.setState({
            isVisible: !this.state.isVisible
        });
    }

    doError() {
        TweenMax.fromTo(this.animationRefs.logo, 0.05, {x:0}, {x:'+=5', repeat:3, yoyo:true, ease:this.easing(Power4.easeInOut)});
    }

    doToggleActive() {
        const opacity = this.state.isActive ? 1 : 0.5;
        TweenMax.to(this.animationRefs.logo, 0.1, {autoAlpha:opacity, ease:this.easing(Quad.easeIn)});
        this.setState({
            isActive: !this.state.isActive
        });
    }
    
    doToggleDrawerLeft() {
        const isOpen = !this.state.leftDrawerOpen;
        this.setState({
            leftDrawerOpen: isOpen
        });

        const listItems = this.animationRefs.leftDrawer.querySelectorAll('.list-item');
        const timeline = new TimelineMax();
        
        if (isOpen) {
            timeline.add( new TweenMax.to(this.animationRefs.leftDrawer, 0.4, {x:'0%', ease:this.easing(Quad.easeOut)}), 0);
            timeline.add( new TweenMax.staggerFromTo(listItems, 0.3, {autoAlpha:0, x:-40}, {autoAlpha:1, x:0, ease:this.easing(Quad.easeInOut)}, 0.05), 0.2);
        } else {
            timeline.add( new TweenMax.staggerTo(listItems, 0.3, {autoAlpha:0, x:-40, ease:this.easing(Quad.easeInOut)}, 0.05), 0);
            timeline.add( new TweenMax.to(this.animationRefs.leftDrawer, 0.4, {x:'-100%', ease:this.easing(Quad.easeInOut)}), 0.2);
        }
    }

    renderAnimationsList() {
        return (
            Animations.map(({type, label, callback}, index) => (
                <StyledListItem key={index}>
                    <StyledButton onClick={this[ callback ]}>{ label }</StyledButton>
                </StyledListItem>
            ))
        );
    }

    render() {

        return (
            <div>
                <Header title="Animation - GSAP" />
                Easing: { this.state.easing && 'on' || 'off' }
                <FlexContainer>
                    <StyledList>{ this.renderAnimationsList() }</StyledList>
                    <AnimationWindow getRefs={this.getRefs} />
                </FlexContainer>
            </div>
        )
    }
};


export default Overview;