// @flow

import React from 'react';
import styled from 'styled-components';

// type LogoProps = {
//     width: string
// };

const LogoWrapper = styled.figure`
`;


const Logo = (props) => {
    const { width } = props;
    return (
        <LogoWrapper innerRef={props.getRef} className={props.className}>
            <svg viewBox="0 0 200 200">
                <path d="M21.5,28.8C-2.3,56.3-7.1,90.8,11,109.4l50.8,52c18.1,18.6,51.8,13.6,78.7-10.8L21.5,28.8z"/>
                <path d="M59.5,49.5l118.9,121.7c23.8-27.5,28.7-62,10.6-80.5l-50.8-52C120.1,20.1,86.4,25.1,59.5,49.5z"/>
            </svg>
        </LogoWrapper>

    );
};

export default Logo;