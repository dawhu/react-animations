import React, { Component } from 'react';
import styled from 'styled-components';

import { COLORS } from '../../constants/styleguide';
import { Title } from '../shared/StyledCopy';

import Logo from '../icons/Logo';

const Window = styled.section`
    position: relative;
    overflow: hidden;

    width: 100%;
    height: auto;
    min-height: 50vh;
    margin: 2vw;

    border-radius: 5px;

    box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);

    background-color: ${COLORS.snow};

    @media (max-width: 767px) {
        margin: 0;
        padding: 2vw;
    }
`;

const PositionedLogo = styled(Logo)`
    position: absolute;

    top: 0;
    right: 0;
    bottom: 0;
    left: 0;

    width: 15vw;
    height: 15vw;

    margin: auto;
`;

type DrawerProps = {
    right: boolean
};
const Drawer = styled.div`
    position: absolute;
    top: 0;
    ${(props: DrawerProps) => {
        if (props.right) return `right: 0; transform: translateX(100%);`;
        return `left: 0; transform: translateX(-100%);`;
    }};
    width: 25%;
    height: 100%;
    padding: 2rem;
    box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
    background-color: ${COLORS.quarts};
`;

const FakeList = styled.ul`
    list-style: none;
    padding: 0;
    margin: 0;
`;

const FakeListItem = styled.li`
    width: ${props => props.width || 100}px;
    height: 15px;
    margin-bottom: 10px;
    background-color: ${COLORS.snow};
`;

class AnimationWindow extends Component {

    componentDidMount() {
        const { getRefs } = this.props;

        getRefs({
            logo: this.logo,
            leftDrawer: this.leftDrawer,
            rightDrawer: this.rightDrawer
        });
    }

    renderListItems() {
        const listItems = [100, 80, 90, 60, 80, 60];
        return listItems.map( (width, index) => <FakeListItem className="list-item" key={index} width={width}/> );
    }

    render() {
        const { onLa } = this.props;
        return(
            <Window>
                <PositionedLogo getRef={logo => this.logo = logo} />
                <Drawer innerRef={drawer => this.leftDrawer = drawer}>
                    <Title size="xl" color={COLORS.snow}>Oberon</Title>
                    <FakeList>
                        { this.renderListItems() }
                    </FakeList>
                </Drawer>
                <Drawer right innerRef={drawer => this.rightDrawer = drawer}>
                </Drawer>
            </Window>
        );
    }
}

export default AnimationWindow;