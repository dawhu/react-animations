// @flow

import React from 'react';
import styled from 'styled-components';
import 'gsap/TweenMax';

export type ImageProps = {
    fill: boolean,
    ratio: number,
    src: string,
    direction: 'top' | 'right' | 'bottom' | 'left'
};

const ImageWrapper = styled.figure`
    position: relative;
    overflow: hidden;
    margin: 0;
    padding-bottom: ${(props: ImageProps) => props.ratio ? `${100/props.ratio}%` : '56.25%'};
    height: ${(props: ImageProps) => props.fill ? '100%' : 'initial'};
`;

const ImageBackground = styled.figure`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    margin: 0;
    background-color: #f0f0f0;
    transform: ${(props: ImageProps) => {
        switch (props.direction) {
            case 'right':
                return 'translateX(100%)';
            
            case 'top':
                return 'translateY(-100%)';
            
            case 'bottom':
                return 'translateY(100%)';

            default:
                return 'translateX(-100%)';
        }
    }}
`;

const Image = styled.img`
    position: absolute;
    width: 100%;
    height: 100%;
    object-fit: cover;

    transform: ${(props: ImageProps) => {
        switch (props.direction) {
            case 'right':
                return 'translateX(120%) scale(1.2)';
            
            case 'top':
                return 'translateY(-120%) scale(1.2)';
            
            case 'bottom':
                return 'translateY(120%) scale(1.2)';

            default:
                return 'translateX(-120%) scale(1.2)';
        }
    }}
`;

const ImageComponent = (props) => {
    const {ratio, fill, src, src2x, direction} = props;
    let background = null;
    let image = null;

    function imageLoadedHandler(e) {
        const timeline = new TimelineMax();
        const duration = 1.2;
        timeline.add( new TweenMax.to( background, duration, {x:'0%', y:'0%', ease:Power4.easeOut}), 0 );
        timeline.add( new TweenMax.to( image, duration, {scale:1, x:'0%', y:'0%', ease:Power4.easeOut}), duration/2 );
    }

    return (
        <ImageWrapper ratio={ratio} fill={fill}>
            <ImageBackground
                direction={direction}
                innerRef={x => background = x} />
            <Image
                direction={direction}
                onLoad={imageLoadedHandler}
                innerRef={x => image = x}
                src={src}
                srcset={`${src} 1x, ${src2x || src} 2x`} />
        </ImageWrapper>
    );
};

export default ImageComponent;