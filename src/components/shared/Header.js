import React from 'react';
import { COLORS } from '../../constants/styleguide';
import { Title, Link } from '../shared/StyledCopy';
import styled from 'styled-components';

const NavList = styled.ul`
    margin: 0;
    padding: 0;
    list-style: none;
`;

const NavButton = styled(Link)`
    position: relative;
    display: inline-block;
    margin-right: 1rem;
    padding: 1rem;
    background-color: ${COLORS.snow};
    
    &.active {
        background-color: ${COLORS.grey};
    }

    label {
        position: relative;
        pointer-events: none;
        color: ${COLORS.quarts};
        z-index:1;
    }

    &:before {
        position: absolute;
        display: block;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;

        background-color: ${COLORS.grey};

        transform: scaleX(0);
        transform-origin: left;
        transition: transform 0.3s ease-in-out;
        content: '';
    }
    
    &:hover {
        &:before {
            transform: scaleX(1);
        }
    }
`;

const Navigation = ( props ) => {
    return (
        <header>
            <Title>{ props.title }</Title>

            <nav>
                <NavList>
                    <NavButton tag="NavLink" exact to='/'><label>Overview</label></NavButton>
                    <NavButton tag="NavLink" exact to='/animations-gsap'><label>Animations GSAP</label></NavButton>
                    <NavButton tag="NavLink" exact to='/animations-react-motion'><label>Animations React-Motion</label></NavButton>
                </NavList>
            </nav>
        </header>
    )
};

export default Navigation;