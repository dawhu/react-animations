// @flow

import React from 'react';
import styled from 'styled-components';
import { Link as RouterLink, NavLink as RouterNavLink } from 'react-router-dom';

import { COLORS, FONTS, FONT_SIZE } from '../../constants/styleguide';

const getProp = (prop, defaultValue) => prop ? prop : defaultValue;
export const validateProps = (props: any, extraInvalidProps?: Array<string>) => {
    const invalidProps = ['tag', 'isCaps', 'weight', 'isInverted', 'hoverColor'].concat( extraInvalidProps || [] );
    const validProps = Object.assign({}, props);
    for (var i = 0, limit = invalidProps.length; i < limit; i++) {
        delete validProps[ invalidProps[ i ] ];
    }
    return validProps;
};

type TextProps = {
    children: any,
    tag: string,
    size: string,
    color: string,
    weight: string,
    isCaps: boolean,
};

/*
 * Title
 * Available props: TextProps
 * 
 * Usage:
 * <Title tag='h1' size='xxl' weight='bold' color='quarts' isCaps={false}>This is a title</Title>
 */
export const Title = styled((props: TextProps) => {
    const validProps = validateProps( props );
    return React.createElement(props.tag || 'h1', validProps, props.children);
})`
    margin: 1.1vw 0;

    font: ${(props: TextProps) => FONTS.primary[props.weight || 'bold']};
    font-size: ${(props: TextProps) => getProp(FONT_SIZE[props.size] || props.size, FONT_SIZE.xxl) };
    text-transform: ${(props: TextProps) => props.isCaps ? 'uppercase' : 'initial'};
    color: ${(props: TextProps) => getProp(COLORS[props.color] || props.color, COLORS.quarts )};
    
    line-height: 1;
`;

/*
 * Subtitle
 * Available props: TextProps
 * 
 * Usage:
 * <Subtitle tag='h2' size='m' weight='light' color='quarts' isCaps={true}>This is a subtitle</Subtitle>
 */
export const Subtitle = styled((props: TextProps) => {
    const validProps = validateProps( props );
    return React.createElement(props.tag || 'h2', validProps, props.children);
})`
    margin: 1.1vw 0;

    font: ${(props: TextProps) => FONTS.primary[props.weight || 'light']};
    font-size: ${(props: TextProps) => getProp(FONT_SIZE[props.size] || props.size, FONT_SIZE.m) };
    text-transform: ${(props: TextProps) => props.isCaps === false ? 'initial' : 'uppercase'};
    color: ${(props: TextProps) => getProp(COLORS[props.color] || props.color, COLORS.quarts )};
    
    line-height: 1;
`;

/*
 * Content
 * Available props: TextProps
 * 
 * Usage:
 * <Content tag='p' size='m' weight='medium' color='quarts' isCaps={false}>This is some content</Content>
 */
export const Content = styled((props: TextProps) => {
    const validProps = validateProps( props );
    return React.createElement(props.tag || 'p', validProps, props.children);
})`
    margin: 0;

    font: ${(props: TextProps) => FONTS.secondary[props.weight || 'regular']};
    font-size: ${(props: TextProps) => getProp(FONT_SIZE[props.size] || props.size, FONT_SIZE.m) };
    text-transform: ${(props: TextProps) => props.isCaps ? 'uppercase' : 'initial'};
    color: ${(props: TextProps) => getProp(COLORS[props.color] || props.color, COLORS.quarts )};
    
    line-height: 1.6;
`;


/*
 * Label
 * Available props: TextProps
 * 
 * Usage:
 * <Label tag='span' size='xl' weight='semibold' color='quarts' isCaps={false}>This is a label</Label>
 */
export const Label = styled((props: TextProps) => {
    const validProps = validateProps( props );
    return React.createElement(props.tag || 'span', validProps, props.children);
})`
    margin: 0;

    font: ${(props: TextProps) => FONTS.primary[props.weight || 'semibold']};
    font-size: ${(props: TextProps) => getProp(FONT_SIZE[props.size] || props.size, FONT_SIZE.xl) };
    text-transform: ${(props: TextProps) => props.isCaps ? 'uppercase' : 'initial'};
    color: ${(props: TextProps) => getProp(COLORS[props.color] || props.color, COLORS.quarts )};
    
    line-height: 1.6;
 `;

 /*
  * Link
  * Available props: LinkProps
  *
  * Usage:
  * <Link to="/route" >
  */

type LinkProps = {
    /*
     * React Router specific
     */
    to: string | Object,
    exact: boolean,
    replace: boolean,

    /*
     * StyledCopy specific
     */
    children: any,
    tag: string,
    size: string,
    color: string,
    hoverColor: string,
    weight: string,
    isCaps: boolean,
};
export const Link = styled((props: LinkProps) => {
    const validProps = validateProps( props );
    if (props.tag === 'NavLink') {
        return <RouterNavLink {...validProps}>{ props.children }</RouterNavLink>
    } else {
        return <RouterLink {...validProps}>{ props.children }</RouterLink>
    }
})`
    margin: 0;

    font: ${(props: LinkProps) => FONTS.primary[props.weight || 'bold']};
    font-size: ${(props: LinkProps) => getProp(FONT_SIZE[props.size] || props.size, FONT_SIZE.m) };
    text-transform: ${(props: LinkProps) => props.isCaps ? 'uppercase' : 'initial'};
    color: ${(props: LinkProps) => getProp(COLORS[props.color] || props.color, COLORS.quarts )};
    letter-spacing: ${(props: LinkProps) => props.isCaps ? '0.1em' : '0'};;

    text-decoration: none;
    
    line-height: 1.6;

    &:hover {
        color: ${(props: LinkProps) => getProp(COLORS[props.hoverColor] || props.hoverColor, COLORS.ruby )};
    }
`;