import { buildApi } from 'redux-bees';

const apiEndpoints = {
};

const config = {
    baseUrl: 'http://localhost:8000'
};

const api = buildApi( apiEndpoints, config );

const statusHelper = ( status ) => {
    if (!status.hasStarted) return 'Request has not started.';
    if (status.isLoading) return 'Loading...';
    if (status.hasFailed) return 'Request failed';
    if (status.error) return 'Request had an error';

    return 'Done';
};

export { statusHelper };

export default api;