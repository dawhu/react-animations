import { createBrowserHistory, createMemoryHistory } from 'history';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import { reducer as beesReducer, middleware as beesMiddleware } from 'redux-bees';
// import thunk from 'redux-thunk';
import rootReducers from '../reducers';

export const isServer = () => typeof window === 'undefined';
export const history = isServer() ? createMemoryHistory : createBrowserHistory;

export default (initialState) => {
    const reducers = combineReducers({
        ...rootReducers,
        bees: beesReducer,
        routing: routerReducer
    });

    const store = createStore(
        reducers,
        initialState,
        compose( 
            applyMiddleware(
                beesMiddleware(),
                routerMiddleware( history )
            ),
            !isServer() && window.devToolsExtension ? window.devToolsExtension() : f => f
        )
    );

    return store;
};